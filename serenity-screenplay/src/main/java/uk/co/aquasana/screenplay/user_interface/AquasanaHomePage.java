package uk.co.aquasana.screenplay.user_interface;

import net.serenitybdd.core.pages.PageObject;
import net.thucydides.core.annotations.DefaultUrl;

@DefaultUrl("https://stage.aquasana.co.uk")
public class AquasanaHomePage extends PageObject {


}