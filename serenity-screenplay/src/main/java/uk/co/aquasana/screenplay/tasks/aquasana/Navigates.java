package uk.co.aquasana.screenplay.tasks.aquasana;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Open;
import uk.co.aquasana.screenplay.actions.Refresh;
import uk.co.aquasana.screenplay.user_interface.AquasanaHomePage;

import static net.serenitybdd.screenplay.Tasks.instrumented;

public class Navigates implements Task {

    private static PageObject page;

    private AquasanaHomePage aquasanaHomePage;

    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Open.browserOn().the(aquasanaHomePage),
                Refresh.theBrowserSession());
    }

    public static Navigates toTheHomepage() {
        return instrumented(Navigates.class);
    }

}