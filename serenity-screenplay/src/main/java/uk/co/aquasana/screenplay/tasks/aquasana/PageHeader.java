package uk.co.aquasana.screenplay.tasks.aquasana;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.targets.Target;
import uk.co.aquasana.screenplay.user_interface.HeaderElements;
import uk.co.aquasana.screenplay.user_interface.ProfilePage;

import static net.serenitybdd.screenplay.Tasks.instrumented;

public class PageHeader implements Task {

    Target expected;
    Target target;

    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
            Click.on(target)
        );
        expected.resolveFor(actor).waitUntilVisible();
    }

    public static PageHeader clicksMyAccount() {
        return instrumented(PageHeader.class, HeaderElements.MY_ACCOUNT_BUTTON, ProfilePage.HEADER);
    }

    public static PageHeader clicksSignOut() {
        return instrumented(PageHeader.class, HeaderElements.SIGN_OUT_BUTTON, HeaderElements.SIGN_IN_BUTTON);
    }

    public PageHeader(Target target, Target expected){
        this.target = target;
        this.expected = expected;
    }
}