package uk.co.aquasana.screenplay.tasks.aquasana;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.serenitybdd.screenplay.actions.Scroll;
import net.thucydides.core.annotations.Step;
import uk.co.aquasana.screenplay.user_interface.GigyaElements;
import uk.co.aquasana.screenplay.user_interface.HeaderElements;

import static net.serenitybdd.screenplay.Tasks.instrumented;

public class Login implements Task {

    private final String username;
    private final String password;

    @Step("{0} logs in with credentials")
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
            Click.on(HeaderElements.SIGN_IN_BUTTON)
        );
        GigyaElements.SUBMIT.resolveFor(actor).waitUntilClickable();
        actor.attemptsTo(
            Enter.theValue(username)
                 .into(GigyaElements.USERNAME),
            Enter.theValue(password)
                 .into(GigyaElements.PASSWORD),
            Scroll.to(GigyaElements.SUBMIT),
            Click.on(GigyaElements.SUBMIT)
        );
    }

    public static Login withCredentials(String username, String password) {
        return instrumented(Login.class, username, password);
    }

    public Login(String username, String password) {
        this.username = username;
        this.password = password;
    }
}