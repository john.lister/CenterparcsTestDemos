package uk.co.aquasana.screenplay.questions.aquasana;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.Text;
import net.serenitybdd.screenplay.waits.WaitUntil;
import uk.co.aquasana.screenplay.user_interface.HeaderElements;

import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.containsText;
import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.isPresent;

public class WelcomeMessage implements Question<String>{

    @Override
    public String answeredBy(Actor actor) {
        WaitUntil.the(HeaderElements.WELCOME_MESSAGE, isPresent()).forNoMoreThan(20).seconds();
        WaitUntil.the(HeaderElements.WELCOME_MESSAGE, containsText("Hi "+actor.getName()));
        return Text.of(HeaderElements.WELCOME_MESSAGE)
                .viewedBy(actor).asString();
    }
}
