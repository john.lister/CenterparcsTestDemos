package uk.co.aquasana.screenplay.questions.aquasana;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.Text;
import net.serenitybdd.screenplay.waits.WaitUntil;
import uk.co.aquasana.screenplay.user_interface.ProfilePage;

import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.isPresent;

public class ProfilePageIsDisplayed implements Question<Boolean>{

    @Override
    public Boolean answeredBy(Actor actor) {
        WaitUntil.the(ProfilePage.HEADER, isPresent()).forNoMoreThan(20);
        return Text.of(ProfilePage.HEADER).viewedBy(actor).asString().equals("My Information");
    }
}
