package uk.co.aquasana.screenplay.questions.aquasana;

import net.serenitybdd.screenplay.Question;

public class Profile {
    public static Question<Boolean> isDisplayed() {
        return new ProfilePageIsDisplayed();
    }
}

