package uk.co.aquasana.screenplay.user_interface;

import net.serenitybdd.screenplay.targets.Target;

public class GigyaElements {
    public static Target USERNAME = Target.the("'Username Field'").locatedBy("//div[@id='gigya-login-screen']//input[@name='username']");
    public static Target PASSWORD = Target.the("'Password Field'").locatedBy("//div[@id='gigya-login-screen']//input[@name='password']");
    public static Target SUBMIT = Target.the("'Submit Button'").locatedBy("//div[@id='gigya-login-screen']//input[@type='submit']");

}
