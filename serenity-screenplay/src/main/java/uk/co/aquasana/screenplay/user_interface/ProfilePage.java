package uk.co.aquasana.screenplay.user_interface;

import net.serenitybdd.screenplay.targets.Target;

public class ProfilePage {
    public static Target HEADER = Target.the("'Profile Page Header'").locatedBy("//a[@class='b-nav-2nd-list__link is-selected']");
}
