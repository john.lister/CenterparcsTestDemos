package uk.co.aquasana.screenplay.user_interface;

import net.serenitybdd.screenplay.targets.Target;

public class HeaderElements {
    public static Target WELCOME_MESSAGE = Target.the("'Welcome message'").locatedBy(".b-list-login__welcome-text");
    public static Target SIGN_IN_BUTTON = Target.the("'Signin Button'").locatedBy(".btn-signin");
    public static Target MY_ACCOUNT_BUTTON = Target.the("'My Account Button'").locatedBy("//li/a[@class='b-list-login__link is-selected']");
    public static Target SIGN_OUT_BUTTON = Target.the("'Signout Button'").locatedBy("//a[@class='b-list-login__link js-logout-user']");
}
