package uk.co.aquasana.screenplay.questions.aquasana;

import net.serenitybdd.screenplay.Question;

public class Header {
    public static Question<String> welcomeMessage() {
        return new WelcomeMessage();
    }
}

