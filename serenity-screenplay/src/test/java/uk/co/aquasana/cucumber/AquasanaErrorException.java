package uk.co.aquasana.cucumber;

public class AquasanaErrorException extends AssertionError {
    public AquasanaErrorException(String message) {
        super(message);
    }

    public AquasanaErrorException(String message, Throwable cause) {
        super(message, cause);
    }
}