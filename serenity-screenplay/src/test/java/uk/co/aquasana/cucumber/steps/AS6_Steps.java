package uk.co.aquasana.cucumber.steps;

import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.serenitybdd.screenplay.actors.OnStage;
import net.serenitybdd.screenplay.actors.OnlineCast;
import uk.co.aquasana.cucumber.AquasanaErrorException;
import uk.co.aquasana.screenplay.actions.BackButton;
import uk.co.aquasana.screenplay.questions.aquasana.Header;
import uk.co.aquasana.screenplay.tasks.aquasana.Login;
import uk.co.aquasana.screenplay.tasks.aquasana.Navigates;
import uk.co.aquasana.screenplay.tasks.aquasana.PageHeader;

import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static net.serenitybdd.screenplay.actors.OnStage.theActorCalled;
import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;
import static org.hamcrest.Matchers.equalTo;

public class AS6_Steps {

    @Before
    public void set_the_stage() {
        OnStage.setTheStage(new OnlineCast());
    }

    @Given("^user (.*) Navigates to relevent Aquasana homepage$")
    public void thatUserNavigatesToAquasanaHomepage(String actorName) throws Throwable {
        theActorCalled(actorName).wasAbleTo(Navigates.toTheHomepage());
    }

    @When("^s?he logs in using \"(.*)\" and \"(.*)\"$")
    public void whenLogsInWithCredentials(String username, String password) throws Throwable {
        theActorInTheSpotlight().attemptsTo(Login.withCredentials(username, password));
    }

    @Then("^s?he is signed in on the home Page$")
    public void thenSignedInToHomepage() throws Throwable {
        String name = theActorInTheSpotlight().getName();
        theActorInTheSpotlight().should(seeThat(Header.welcomeMessage(), equalTo("Hi "+name))
                .orComplainWith(AquasanaErrorException.class, name + " is not logged in."));
    }

    @When("^s?he clicks on My Account")
    public void whenUserClicksOnMyAccount() throws Throwable {
        theActorInTheSpotlight().attemptsTo(PageHeader.clicksMyAccount());
    }

    @And("^s?he clicks the back button")
    public void andUserClicksBackButton() throws Throwable {
        theActorInTheSpotlight().attemptsTo(BackButton.press());
    }

    @And("^s?he clicks Sign-Out")
    public void whenUserClicksSignOut() throws Throwable {
        theActorInTheSpotlight().attemptsTo(PageHeader.clicksSignOut());
    }

}
