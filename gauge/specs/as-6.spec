Aquasana AS-6
================

* Goto "Aquasana" homepage

Login to Aquasana
----------------
tags: aquasana, as-6, login

* Log in with customer "Test" using "jsl@mailinator.com" and "P@ss1234"
* Check if the user "Test" is signed in
* Visit the account page
* Press the back button
* Sign out the user
* Check if the user "Test" is signed out
