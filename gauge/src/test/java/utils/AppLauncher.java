package utils;

import com.thoughtworks.gauge.Step;
import utils.driver.Driver;

public class AppLauncher {

    @Step("Go to the <url_name> website")
    public void launchTheApplication(String urlName) {
        String url = System.getenv(urlName.toUpperCase() + "_URL");
        Driver.webDriver.get(url);
    }
}