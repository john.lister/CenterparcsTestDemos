package utils;

import com.thoughtworks.gauge.Step;
import utils.driver.Driver;

public class BrowserAction {

    @Step("Press the back button")
    public void pressTheBackButton() {
        Driver.webDriver.navigate().back();
    }
}