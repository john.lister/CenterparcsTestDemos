import com.thoughtworks.gauge.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import utils.driver.Driver;

public class Account {
    @Step("Visit the account page")
    public void visitTheAccountPage() {
        WebDriver webDriver = Driver.webDriver;
        webDriver.findElement(By.xpath("//li/a[@class='b-list-login__link is-selected']")).click();
    }
}
