import com.thoughtworks.gauge.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import utils.driver.Driver;

import static org.junit.Assert.assertTrue;

public class LogIn {

    @Step("Give an option to Log In")
    public void giveAnOptionToLogIn() {
        WebDriver webDriver = Driver.webDriver;
        WebDriverWait wait = new WebDriverWait(webDriver, 5);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("btn-signin")));
        WebElement logIn = webDriver.findElement(By.className("btn-signin"));
        assertTrue(logIn.isDisplayed());
    }

    @Step("Show the log in status for user <customer>")
    public void showTheLogInStatusForUser(String customer) {
        String welcomeText = "Hi "+customer;
        WebDriver webDriver = Driver.webDriver;
        WebDriverWait wait = new WebDriverWait(webDriver, 10);
        wait.until(ExpectedConditions.textToBe(By.className("b-list-login__welcome-text"), welcomeText));
        WebElement authenticatedInfo = webDriver.findElement(By.className("b-list-login__welcome-text"));
        assertTrue(authenticatedInfo.isDisplayed());
    }

    @Step("Login as name <name> and <password>")
    public void LoginAsCustomerDetails(String name, String password) {
        WebDriver webDriver = Driver.webDriver;
        webDriver.findElement(By.className("btn-signin")).click();
        WebDriverWait wait = new WebDriverWait(webDriver, 10);
        WebElement submitButton = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div[@id='gigya-login-screen']//input[@type='submit']")));
        webDriver.findElement(By.xpath("//div[@id='gigya-login-screen']//input[@name='username']")).sendKeys(name);
        webDriver.findElement(By.xpath("//div[@id='gigya-login-screen']//input[@name='password']")).sendKeys(password);
        submitButton.click();
    }
}
